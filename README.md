# velhop
CTS Velhop available bike park places - OpenData API

- [CTS OpenDATA](https://www.cts-strasbourg.eu/fr/portail-open-data)

- [Velhop XML live data sheet](http://velhop.strasbourg.eu/tvcstations.xml)
