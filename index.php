<?php
include_once(getcwd().'/config.php');
include_once(getcwd().'/functions/curling.php');

if(!$smarty->isCached('index.html', $page_unique_id)) {
    $aMessageSucces = array();
	$aMessageError  = array();

	$velhop = curling($CTS_token); // use curling($CTS_token, 1) to request "veloparc";

	if (!isset($velhop) || empty($velhop)) {
		$aMessageError[] = "Une erreur est survenue. Merci de ressayer dans quelques instants. <br> Si le problème persiste merci de contacter <a href='mailto:natan.fourie@hotmail.fr'>l'administrateur</a>.";
		$smarty->assign('aMessageError', $aMessageError);
		$smarty->display('error404.html');
		exit();
	}
	foreach ($velhop as $key => $station) {
		if ($key >= 2) {
			$aStationInformations = array (
				"Designation" 			 => substr($station->Designation, 4),
				"Longitude" 			 => $station->Longitude,
				"Latitude" 				 => $station->Latitude,
				"StationID" 			 => $station->StationID,
				"AvailableBikes" 		 => $station->AvailableBikes,
				"AvailableFreeBikeSpots" => $station->AvailableFreeBikeSpots,
				"TotalBikeSpots" 		 => $station->TotalBikeSpots,
				"BankCard" 				 => $station->BankCard,
				"AccessInformation" => [
					"EN" => $station->AccessInformation_EN,
					"FR" => $station->AccessInformation_FR,
					"DE" => $station->AccessInformation_DE,
				],
				"ServiceType" => ""
			);
			$aStationInformations["BankCard"] = ($aStationInformations["BankCard"] == 1) ? "oui" : "non";
			
			$sLat = $aStationInformations["Latitude"];
			$sLon = $aStationInformations["Longitude"];
			
			// Choose your preferred maps API option.
			require getcwd().'/assets/api/maps/bingMaps.php';
			// require getcwd().'/assets/api/maps/googleMaps.php';

			if ($aStationInformations["stationAddress"] == "16 Avenue de l'Europe, 67300 Schiltigheim" || $aStationInformations["stationAddress"] == "2 Rue de Dublin, 67300 Schiltigheim") $aStationInformations["stationAddress"] = "1 Place de Paris, 67300 Schiltigheim";

			$aStationInformationsDisplayTable[] = $aStationInformations;
		}
	}

    $smarty->assign('aMessageSucces', $aMessageSucces);
	$smarty->assign('aMessageError', $aMessageError);
	$smarty->assign('aStationInformationsDisplayTable', $aStationInformationsDisplayTable);
}

$smarty->display('index.html', $page_unique_id);
