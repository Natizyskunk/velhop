<?php
function curling(string $token, int $type = 0) : array {
	GLOBAL $smarty;

	$api = ($type === 1) ? "https://api.cts-strasbourg.eu/v1/cts/veloparc" : "https://api.cts-strasbourg.eu/v1/velhop/velhop";

	$ch  = curl_init();
	curl_setopt($ch, CURLOPT_URL, $api);
	if (!empty($token) && $token !== 0) {
		curl_setopt($ch, CURLOPT_USERPWD, $token);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	}
	if (preg_match('`^https://`i', $api)) {
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	}
	curl_setopt($ch, CURLOPT_HTTPHEADER, [
		'Content-encoding: gzip',
		'Content-type: application/json; charset=utf-8',
		'Accept: text/plain,text/json,text/xml,application/json,application/xml;'
	]);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 0);

	if (curl_errno($ch)) {
		// $aMessageError[] = "Error: ".curl_error($ch);
		$aMessageError[] = "Une erreur est survenue. Merci de ressayer dans quelques instants. <br> Si le problème persiste merci de contacter <a href='mailto:natan.fourie@hotmail.fr'>l'administrateur</a>.";
		$smarty->assign('aMessageError', $aMessageError);
		$smarty->display('error404.html');
		curl_close($ch);
		exit();
	}

	$resp = curl_exec($ch);
	$resp_json = json_decode($resp);
	curl_close($ch);

	/* echo '<pre>';
	// foreach ($velhop as $key => $station) {
	// 	if ($key == 2) {
	// 		print_r($station);
	// 		echo '<hr>';
	// 	}
	// }
	print_r($resp_json);
	echo '</pre>'; */

	return (array) $resp_json->Velhop;
};