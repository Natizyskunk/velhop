<?php
$sReverseGeoCodeUrl = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$sLat.','.$sLon.'&key='.$sGoogleMapsAPIKey;
$sGeoCodeContent = file_get_contents($sReverseGeoCodeUrl); // Need to change it to cURL !!
$json = json_decode($sGeoCodeContent, true);
$sStationAddress = $json['results'][0]['formatted_address'];
$aStationInformations["stationAddress"] = $sStationAddress;
// $aStationInformations["stationAddress"] = substr($sStationAddress, 0, -8);