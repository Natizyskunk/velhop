<?php
$sReverseGeoCodeUrl = 'http://dev.virtualearth.net/REST/v1/Locations/'.$sLat.','.$sLon.'?&o=json&key='.$sBingMapsAPIKey;
$sBingMapsContent = file_get_contents($sReverseGeoCodeUrl); // Need to change it to cURL !!
$json = json_decode($sBingMapsContent, true);
$sStationAddress = $json['resourceSets'][0]['resources'][0]['address']['formattedAddress'];
$aStationInformations["stationAddress"] = $sStationAddress;