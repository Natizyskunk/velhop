(function(){
    "use strict";

    $('#myTable').DataTable({
        "paging":   true,
        "pagingType": "full_numbers",
        "ordering": true,
        "order":    [[ 0, "asc" ]],
        "info":     false,
        // "scrollY": "70vh",
        "scrollCollapse": "true",
        "language": {
            "decimal":        "",
            "emptyTable":     "Aucune donnée disponible dans le tableau",
            "info":           "Affiche _START_ à _END_ entrées sur _TOTAL_",
            "infoEmpty":      "Affiche 0 à 0 entrées sur 0",
            "infoFiltered":   "(filtrer from _MAX_ total entrées)",
            "infoPostFix":    "",
            "thousands":      ",",
            "lengthMenu":     "Affiche _MENU_ entrées",
            "loadingRecords": "Chargement...",
            "processing":     "Traitement...",
            "search":         "recherche:",
            "zeroRecords":    "Aucun résultat disponible",
            "paginate": {
                "first":    "&#171;", // "«"
                "previous": "&lt;", // "‹" or "&#60;"
                "next":     "&gt;", // "›" or "&#62;"
                "last":     "&#187;" // "»"
            },
            "aria": {
                "sortAscending":  ": activer pour trier la colonne de maniere ascending",
                "sortDescending": ": activer pour trier la colonne de maniere descending",
                "paginate": {
                    "first":    "Premier",
                    "previous": "Précédent",
                    "next":     "Suivant",
                    "last":     "Dernier"
                }
            }
        }
    });

    // var table = $('#myTable').DataTable();

    /* $('#myTable_wrapper tbody').on('click', 'tr', function () {
        var data = table.row( this ).data();
        alert( 'Station: ' + data[1] + '. \n'
            + 'Adresse: ' + data[2] + '. \n'
            + 'Vélos disponibles: ' + data[3] + '. \n'
            + 'Vélos utilisés: ' + data[4] + '. \n'
            + 'Total vélos: ' + data[5] + '. \n'
            + 'CB acceptée: ' + data[6] + '.'
        );
    }); */
})();
