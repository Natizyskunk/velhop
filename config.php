<?php
// Activation du strict mode.
declare(strict_types=1);

// Définition du fuseau horaire.
date_default_timezone_set('Europe/Paris');

// Gestion et affichage des erreurs PHP.
ini_set('display_errors', 'on'); //! Passer à "on" en Dev & changer à "off" quand passage en prod.
ini_set('error_reporting', 'E_ALL'); //! Paser à "E_ALL" en Dev et changer à "0" quand passage en prod.

header("Cache-Control: no-cache");

// Initialisation Smarty
require_once(getcwd()."/assets/Smarty/Smarty.class.php");
$smarty = new Smarty();
$smarty->compile_check  = true;
$smarty->force_compile  = true;
$smarty->caching        = 0; //! Le cache est désactivé.
$smarty->cache_lifetime = 0; //! Le cache n'est jamais (re)généré (paramètre en secondes).
// $smarty->caching        = 1; //! Le cache est activé en prod.
// $smarty->cache_lifetime = 10; //! Le cache est (re)généré toutes les 10 secondes en prod.
// $smarty->debugging      = true; //! Décommenter cette ligne pour accéder à la console de debugging de Smarty.

$smarty->setTemplateDir(getcwd().'/view/templates/');
$smarty->setCompileDir(getcwd().'/view/templates_c/');
$smarty->setConfigDir(getcwd().'/view/configs/');
$smarty->setCacheDir(getcwd().'/view/cache/');
$smarty->setPluginsDir(getcwd().'/view/plugins/');

// Récupérer les différents indentifiants pour l'appli
include_once(getcwd().'/credentials.php');
